<?php
  if (version_compare(phpversion(), '5.0.0', '>=') && version_compare(phpversion(), '7.0.0', '<')) {
    echo 'Hello, World! I am in php5 (v' . PHP_VERSION . " detected)\n";
  } else {
      throw new Exception('I am not in php5 (v' . PHP_VERSION . ' detected)');
  }
