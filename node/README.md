# Node
## Requirement
This project requires Node.

No specific version required, try to use a small node image.

## How to run
```bash
node hello-world.js
```
This will display a hello world message.
